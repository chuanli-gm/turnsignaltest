
//import shared_enums;
import java.io.*;
import lcm.lcm.*;
import exlcm.behaviorplan_t;
import java.util.Scanner;


public class BehaviorPlanGenerator implements LcmMessageGenerator{
	private LCM lcm;
	static private int target_speed = 0;

	public BehaviorPlanGenerator() {
		lcm = LcmUtility.getLcm();
	}

	public void GenerateDataAndPublish(int i) {

		behaviorplan_t behaviorplan = new behaviorplan_t();
		if (i % 8 == 0) {
			System.out.println("Setting target speed to 30 MPH");
			behaviorplan.speed_target_mps = 30;
			target_speed = 30;
		} else if (i % 8 == 1) {
			behaviorplan.speed_target_mps = 15;
			target_speed = 15;
			System.out.println("Setting target speed to 15 MPH");
		} else if (i % 8 == 2) {
			behaviorplan.speed_target_mps = 0;
			target_speed = 0;
			System.out.println("Setting target speed to 0 MPH");
		} else if (i % 8 == 3) {
			behaviorplan.active_maneuver = (short) shared_enums.eActiveManeuver.LANE_KEEPING_MANEUVER.value;
			behaviorplan.speed_target_mps = target_speed;
			System.out.println("Setting maneuver to LANE_KEEPING type");
		} else if (i % 8 == 4) {
			behaviorplan.behavior_type = (short) shared_enums.eBehaviorType.TURN_LEFT_BEHAVIOR.value;
			behaviorplan.speed_target_mps = target_speed;
			System.out.println("Setting maneuver to TURN_LEFT type");
			// behaviorplan.speed_target_mps = 30;
		} else if (i % 8 == 5) {
			behaviorplan.behavior_type = (short) shared_enums.eBehaviorType.TURN_RIGHT_BEHAVIOR.value;
			behaviorplan.speed_target_mps = target_speed;
			System.out.println("Setting maneuver to TURN_RIGHT type");
			// behaviorplan.speed_target_mps = 30;
		} else if (i % 8 == 6) {
			// behaviorplan.speed_target_mps = 30;
			behaviorplan.left_lane_intention = (short) shared_enums.eLaneIntention.LANE_CHANGE_IS_MANDATORY.value;
			behaviorplan.speed_target_mps = target_speed;
			System.out.println("Setting maneuver to LEFT_LANE_CHANGING_IS_MANDATORY type");
		} else if (i % 8 == 7) {
			// behaviorplan.speed_target_mps = 30;
			behaviorplan.right_lane_intention = (short) shared_enums.eLaneIntention.LANE_CHANGE_IS_MANDATORY.value;
			behaviorplan.speed_target_mps = target_speed;
			System.out.println("Setting maneuver to RIGHT_LANE_CHANGE_IS_MANDATORY type");
		}
		lcm.publish("BEHAVIORPLAN", behaviorplan);
	}

  public static void main(String[] args) {
    System.out.println ("Usage: \n \t0: Set target speed to 30 m/s \n\t 1: Set target speed to 15 m/s \n\t 2: Set target speed to 0 m/s \n\t 3: Set maneuver to LANE_KEEPING type \n\t 4: Set maneuver to TURN_LEFT type \n\t 5: Set maneuver to TURN_RIGHT type \n\t 6: Set maneuver to LEFT_LANE_CHANGING_IS_MANDATORY type \n\t 7: Set maneuver to RIGHT_LANE_CHANGING_IS_MANDATORY type");
    while (true){
      Scanner scan = new Scanner (System.in);
      int i = scan.nextInt ();
      System.out.println ("Your input is: " + i);
      BehaviorPlanGenerator bpg = new BehaviorPlanGenerator ();
      bpg.GenerateDataAndPublish(i);
    }
  }

}

