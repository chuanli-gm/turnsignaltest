/* LCM type definition class file
 * This file was automatically generated by lcm-gen
 * DO NOT MODIFY BY HAND!!!!
 */

package exlcm;
 
import java.io.*;
import java.util.*;
import lcm.lcm.*;
 
public final class behaviorplan_t implements lcm.lcm.LCMEncodable
{
    public short entity_type;
    public byte version_n;
    public byte valid_f;
    public double timestamp_sec;
    public long ref_n;
    public short master_state;
    public short state_goal;
    public short active_maneuver;
    public short behavior_type;
    public short left_lane_intention;
    public short right_lane_intention;
    public float lane_change_zone_start_m;
    public float lane_change_zone_end_m;
    public float speed_target_mps;
    public float speed_target_distance_m;
    public float lateral_accel_lim_mpss;
    public float longitudinal_accel_lim_mpss;
    public float longitudinal_decel_lim_mpss;
    public boolean trajectory_rejected_by_controller;
    public boolean cib_active_f;
    public boolean manual_override_f;
    public boolean reroute_needed_f;
 
    public behaviorplan_t()
    {
    }
 
    public static final long LCM_FINGERPRINT;
    public static final long LCM_FINGERPRINT_BASE = 0x44fdfb4144026979L;
 
    static {
        LCM_FINGERPRINT = _hashRecursive(new ArrayList<Class<?>>());
    }
 
    public static long _hashRecursive(ArrayList<Class<?>> classes)
    {
        if (classes.contains(exlcm.behaviorplan_t.class))
            return 0L;
 
        classes.add(exlcm.behaviorplan_t.class);
        long hash = LCM_FINGERPRINT_BASE
            ;
        classes.remove(classes.size() - 1);
        return (hash<<1) + ((hash>>63)&1);
    }
 
    public void encode(DataOutput outs) throws IOException
    {
        outs.writeLong(LCM_FINGERPRINT);
        _encodeRecursive(outs);
    }
 
    public void _encodeRecursive(DataOutput outs) throws IOException
    {
        outs.writeShort(this.entity_type); 
 
        outs.writeByte(this.version_n); 
 
        outs.writeByte(this.valid_f); 
 
        outs.writeDouble(this.timestamp_sec); 
 
        outs.writeLong(this.ref_n); 
 
        outs.writeShort(this.master_state); 
 
        outs.writeShort(this.state_goal); 
 
        outs.writeShort(this.active_maneuver); 
 
        outs.writeShort(this.behavior_type); 
 
        outs.writeShort(this.left_lane_intention); 
 
        outs.writeShort(this.right_lane_intention); 
 
        outs.writeFloat(this.lane_change_zone_start_m); 
 
        outs.writeFloat(this.lane_change_zone_end_m); 
 
        outs.writeFloat(this.speed_target_mps); 
 
        outs.writeFloat(this.speed_target_distance_m); 
 
        outs.writeFloat(this.lateral_accel_lim_mpss); 
 
        outs.writeFloat(this.longitudinal_accel_lim_mpss); 
 
        outs.writeFloat(this.longitudinal_decel_lim_mpss); 
 
        outs.writeByte( this.trajectory_rejected_by_controller ? 1 : 0); 
 
        outs.writeByte( this.cib_active_f ? 1 : 0); 
 
        outs.writeByte( this.manual_override_f ? 1 : 0); 
 
        outs.writeByte( this.reroute_needed_f ? 1 : 0); 
 
    }
 
    public behaviorplan_t(byte[] data) throws IOException
    {
        this(new LCMDataInputStream(data));
    }
 
    public behaviorplan_t(DataInput ins) throws IOException
    {
        if (ins.readLong() != LCM_FINGERPRINT)
            throw new IOException("LCM Decode error: bad fingerprint");
 
        _decodeRecursive(ins);
    }
 
    public static exlcm.behaviorplan_t _decodeRecursiveFactory(DataInput ins) throws IOException
    {
        exlcm.behaviorplan_t o = new exlcm.behaviorplan_t();
        o._decodeRecursive(ins);
        return o;
    }
 
    public void _decodeRecursive(DataInput ins) throws IOException
    {
        this.entity_type = ins.readShort();
 
        this.version_n = ins.readByte();
 
        this.valid_f = ins.readByte();
 
        this.timestamp_sec = ins.readDouble();
 
        this.ref_n = ins.readLong();
 
        this.master_state = ins.readShort();
 
        this.state_goal = ins.readShort();
 
        this.active_maneuver = ins.readShort();
 
        this.behavior_type = ins.readShort();
 
        this.left_lane_intention = ins.readShort();
 
        this.right_lane_intention = ins.readShort();
 
        this.lane_change_zone_start_m = ins.readFloat();
 
        this.lane_change_zone_end_m = ins.readFloat();
 
        this.speed_target_mps = ins.readFloat();
 
        this.speed_target_distance_m = ins.readFloat();
 
        this.lateral_accel_lim_mpss = ins.readFloat();
 
        this.longitudinal_accel_lim_mpss = ins.readFloat();
 
        this.longitudinal_decel_lim_mpss = ins.readFloat();
 
        this.trajectory_rejected_by_controller = ins.readByte()!=0;
 
        this.cib_active_f = ins.readByte()!=0;
 
        this.manual_override_f = ins.readByte()!=0;
 
        this.reroute_needed_f = ins.readByte()!=0;
 
    }
 
    public exlcm.behaviorplan_t copy()
    {
        exlcm.behaviorplan_t outobj = new exlcm.behaviorplan_t();
        outobj.entity_type = this.entity_type;
 
        outobj.version_n = this.version_n;
 
        outobj.valid_f = this.valid_f;
 
        outobj.timestamp_sec = this.timestamp_sec;
 
        outobj.ref_n = this.ref_n;
 
        outobj.master_state = this.master_state;
 
        outobj.state_goal = this.state_goal;
 
        outobj.active_maneuver = this.active_maneuver;
 
        outobj.behavior_type = this.behavior_type;
 
        outobj.left_lane_intention = this.left_lane_intention;
 
        outobj.right_lane_intention = this.right_lane_intention;
 
        outobj.lane_change_zone_start_m = this.lane_change_zone_start_m;
 
        outobj.lane_change_zone_end_m = this.lane_change_zone_end_m;
 
        outobj.speed_target_mps = this.speed_target_mps;
 
        outobj.speed_target_distance_m = this.speed_target_distance_m;
 
        outobj.lateral_accel_lim_mpss = this.lateral_accel_lim_mpss;
 
        outobj.longitudinal_accel_lim_mpss = this.longitudinal_accel_lim_mpss;
 
        outobj.longitudinal_decel_lim_mpss = this.longitudinal_decel_lim_mpss;
 
        outobj.trajectory_rejected_by_controller = this.trajectory_rejected_by_controller;
 
        outobj.cib_active_f = this.cib_active_f;
 
        outobj.manual_override_f = this.manual_override_f;
 
        outobj.reroute_needed_f = this.reroute_needed_f;
 
        return outobj;
    }
 
}

