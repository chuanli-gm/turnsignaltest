
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;
import javax.xml.xpath.*;
import java.io.*;

public class SysParameters
{
  private static String path;
  private static Document document;
  private static SysParameters instance = null;
  public static void setPath (String path)
  {
    path = path;
  }
  private SysParameters ()
  {
  }
  public static SysParameters getInstance ()
  {
    if (instance == null )
    {
      instance = new SysParameters ();
    }
    return instance;
  }
  public static void init (String path)
  {
    path = path;
    DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance ();
    DocumentBuilder builder = null;
    try 
    {
      builder = builderFactory.newDocumentBuilder ();
    }
    catch (ParserConfigurationException e)
    {
      System.out.println ("Exception " + e);
    }
    try 
    {
      document = builder.parse (new FileInputStream (path));
    }
    catch (SAXException e)
    {
      System.out.println ("Exception " + e);
    }
    catch (IOException e)
    {
      System.out.println ("Exception " + e);
    }
  }

  /**
   * str should be a valid xpath expression
   */
  String getParameter (String str)
  {
    String val = "";
    try
    {
      XPath  xPath = XPathFactory.newInstance ().newXPath ();
      val = xPath.compile (str).evaluate (document);
    }
    catch (XPathExpressionException e)
    {
      System.out.println ("Exception " + e + " xpath: " + str);
    }
    finally 
    {
      return val;
    }
  }
}
